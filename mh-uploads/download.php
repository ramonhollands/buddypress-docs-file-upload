<?php

/*add_action('wp_ajax_mh_download_file', 'mh_download_file_callback');
add_action( 'wp_ajax_nopriv_mh_download_file', 'mh_download_file_callback' );
*/

add_action('wp','mh_download_file_callback');
function mh_download_file_callback() {
	if ( get_query_var('mhdw') != '' && get_query_var('dwid') != '' && get_query_var('dwy') != '' && get_query_var('dwm') != '') {
		global $bp;
		/*$group_id = get_query_var('gpid');
		$group2 = groups_get_group( array( 'group_id' => $group_id ) ); 
			// If the group is not public, then the user must be logged in and
			// a member of the group to download the document
		if ( $group2->status != 'public' ) {
			if ( !is_user_logged_in() || !groups_is_user_member( bp_loggedin_user_id(), $group_id ) ) {
				$error = array(
					'message' 	=> sprintf( 'You must be a logged-in member of the group %s to access this document. If you are a member of the group, please log into the site and try again.', $group2->name ),
					'redirect'	=> bp_get_group_permalink( $group2 )
				);
			}
		} else {
			if ( !is_user_logged_in() ) {
				$error = array(
					'message' 	=> sprintf( 'You must be a logged-in to access this document. Please log into the site and try again.'),
					'redirect'	=> bp_get_group_permalink( $group2 )
				);
			}	
		}*/
		if ( !$error ) {
		$filez = get_query_var('mhdw');
		$filez_id = get_query_var('dwid');
		$filez_year = get_query_var('dwy');
		$filez_month = get_query_var('dwm');
		$doc_path = ABSPATH . '/group/uploads/' . $filez_year . '/' . $filez_month . '/' . $filez_id . '/' . $filez;
		$doc_size  = filesize($doc_path);
			if ( file_exists( $doc_path ) ) {
				$mime_type = mime_content_type( $doc_path );
				$doc_size = filesize( $doc_path );
				
				header("Cache-Control: public, must-revalidate, post-check=0, pre-check=0");
				header("Pragma: hack");
					
				header("Content-Type: $mime_type; name='" . $filez . "'");
				header("Content-Length: " . $doc_size );
				
				header('Content-Disposition: attachment; filename="' . $filez . '"');
				header("Content-Transfer-Encoding: binary");              
				ob_clean();
				flush();  		
				readfile( $doc_path );
				die();
		       
			} else {
				// File does not exist
				$error = array(
					'message' 	=> sprintf( 'File Doesn\'t Exist.', $group->name ),
					'redirect'	=> bp_get_group_permalink( $group )
				);
			}
		}
		
		bp_core_add_message( $error['message'], 'error' );
		bp_core_redirect( $error['redirect'] );
	}
}

if(!function_exists('mime_content_type')) {

    function mime_content_type($filename) {

        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        $ext = strtolower(array_pop(explode('.',$filename)));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        }
        elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        }
        else {
            return 'application/octet-stream';
        }
    }
}
?>
