<?php

add_action('wp_ajax_mh_uploads_ajax', 'mh_uploads_ajax_callback');
add_action( 'wp_ajax_nopriv_mh_uploads_ajax', 'mh_uploads_ajax_callback' );

function mh_uploads_ajax_callback() {
	$rand = time() . rand(0,100000);
	$path4 = ABSPATH . 'group/uploads/' . date('Y') . '/' . date('m') . '/' . $rand . '/' ; 
	if(!file_exists($path4)) mkdir($path4 , 0777, true);
	 if(get_option('max_upload_size') == '') { $max_filesize = 10485760; }
	 else { $max_filesize = get_option('max_upload_size'); }

	$allowed_filetypes = array('odt','rtf','txt','doc','docx','xls','xlsx','ppt','pps','pptx','pdf','jpg','jpeg','gif','png','zip','tar','gz'); 
	$settings = get_mh_upload_settings();
	 // Maximum filesize in BYTES.
	  // These will be the types of file that will pass the validation.
    $fileControlName = 'qqfile'; //userfile
	$filename = $_FILES[$fileControlName]['name']; // Get the name of the file (including file extension).
	$ext = pathinfo($filename, PATHINFO_EXTENSION); // Get the extension from the filename.
	$file_strip = str_replace(" ","_",$filename); //Strip out spaces in filename
	$upload_path = $path4; //Set upload path
	$pimg = get_bloginfo('url') . '/downloads/' . date('Y') . '/' . date('m') . '/' . $rand.'/' . $file_strip;  
	 // Check if the filetype is allowed, if not DIE and inform the user.
	if (!in_array($ext,$allowed_filetypes)) {
		$result = array('error' => 'failed', 'message' => $settings['EXTENSION_ERROR']);
        echo json_encode($result);
        die();
	}
   // Now check the filesize, if it is too large then DIE and inform the user.
   if (filesize($_FILES[$fileControlName]['tmp_name']) > $max_filesize) {
        $result = array('error' => 'failed', 'message' => str_replace("%max_filesize%", $max_filesize/(1048576), $settings['FILESIZE_ERROR']));
        echo json_encode($result);
        die();
 	}
   // Check if we can upload to the specified path, if not DIE and inform the user.
   if (!is_writable($upload_path)) {
        $result = array('error' => 'failed', 'message' => str_replace("%path%", $path4, $settings['FOLDER_PERMISSION']));
        echo json_encode($result);
        die();
	}
	 // Move the file if eveything checks out.
	 if (move_uploaded_file($_FILES[$fileControlName]['tmp_name'], $upload_path . $file_strip)) {
	 $imgz = array('jpg', 'jpeg', 'gif', 'png');
	 if (in_array($ext, $imgz)) {
        $type = 'image';
		$codez = '<img src="' . $pimg . '" />';
	 } else {
        $type = 'file';
		$codez = '<a href="' . $pimg . '">' . $file_strip . '</a>';
	 }
	 
        $result = array('success' => 'ok', 'message' => str_replace("%filename%", $file_strip, $settings['UPLOAD_SUCCESS']), 'insert_type' => $type, 'insert_link' => $pimg, 'insert_name' => $file_strip);
        echo json_encode($result);
        die();
    } else {
        $result = array('error' => 'true', 'message' => str_replace("%filename%", $file_strip, $settings['UPLOAD_ERROR']));
        echo json_encode($result);
        die();
	}
	die();
}

?>