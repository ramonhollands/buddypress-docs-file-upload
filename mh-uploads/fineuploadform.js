jQuery(document).ready(function() {
    var manualuploader = new qq.FineUploader({
      element: jQuery('#manual-fine-uploader')[0],
      request: {
        endpoint: jQuery('#docfile_path').val(),
        forceMultipart: true
      },
      autoUpload: false,
      multiple: false,
      text: {
        uploadButton: '<i class="icon-plus icon-white"></i> Selecteer Bestanden',
        cancelButton: 'Annuleren',
        retryButton: 'Opnieuw proberen',
        failUpload: 'Upload mislukt',
        dragZone: 'Drop bestanden hier om te uploaden',
        formatProgress: "{percent}% of {total_size}",
        waitingForResponse: "Verwerking..."
      },
      callbacks: {
        onComplete: function(id, fileName, responseJSON) {
          if (responseJSON.success){
            var mh_type = responseJSON.insert_type;
            var mh_link = responseJSON.insert_link;
            var mh_name = responseJSON.insert_name;
            if (mh_type != '' && mh_link != '') {
              var mh_insert = mh_type == 'image' ? '<img src="' + mh_link + '" />' : '<a href="' + mh_link + '">' + mh_name + '</a>';
              var editor = tinyMCE.get("doc_content");
              editor.execCommand('mceInsertContent', false, mh_insert);
              jQuery('#mh-uploads-box').trigger('close');
            }
          }
        },
        onError: function(id, fileName, reason) {
          //alert('Error');
        },
        onValidate: function() {
          //alert('Validate');
        }
      },
      failedUploadTextDisplay: {
        mode: 'custom',
        maxChars: 100,
        responseProperty: 'message',
        enableTooltip: true
      },
      debug: true
    });
 
    jQuery('#triggerUpload').click(function() {
      manualuploader.uploadStoredFiles();
    });
});