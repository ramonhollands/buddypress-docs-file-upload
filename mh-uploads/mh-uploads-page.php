<?php 
/* Admin Panel Options Page */

?>


	<?php if(isset($_POST['save_settings_mh_tabs'])) { update_option('max_upload_size',$_POST['max_upload_size']); update_option('max_files_upload',$_POST['max_files_upload']); update_option('allowed_mh_uploads',$_POST['allowed_mh_uploads']); 
														$mh_status = 'Settings Saved'; 
	$msgz = $_POST['msgz'];
	//print_r($msgz);
	save_mh_upload_settings($msgz);													
	} ?>
		<div class="wrap">
			<div id="icon-wp-polls" class="icon32"><br /></div>
			<h2>MH Group Uploads</h2><br/>
<?php		if(isset($mh_status)) echo '<div class="updated settings-error" id="setting-error-settings_updated"> 
<p><strong>'.$mh_status.'</strong></p></div>'; ?>

<form method="post" action="<?php echo admin_url('admin.php?page=mh_group_uploads'); ?>">
<table class="form-table">
	
		<tr>
			<th width="20%" scope="row" valign="top" >Max Size: </th>
 			<td width="80%"><input type="text" size="2" name="max_upload_size" value="<?php echo get_option('max_upload_size'); ?>" /> in bytes</td>
		</tr>
		<tr>
			<th width="20%" scope="row" valign="top" >Max Files Upload: </th>
 			<td width="80%"><input type="text" size="2" name="max_files_upload" value="<?php echo get_option('max_files_upload'); ?>" /> Number of files</td>
		</tr>
		<tr>
			<th width="20%" scope="row" valign="top" >Allowed File Types: </th>
 			<td width="80%"><input type="text" size="40" name="allowed_mh_uploads" value="<?php echo get_option('allowed_mh_uploads'); ?>" /> 'pdf','png'...... (seprated with comma)</td>
		</tr>
	<?php 
	$msgz = get_mh_upload_settings();
	foreach ($msgz as $key => $value) { ?>
		<tr>
			<th width="20%" scope="row" valign="top"><?php echo str_replace("_"," ",$key); ?></th>
			<td width="80%"><input type="text" size="70" name="msgz[<?php echo $key; ?>]" value="<?php echo $value; ?>" /></td>
		</tr>
	<?php } ?>
		
	</table>
	<br/>
	<p style="text-align: center;"><input type="submit" name="save_settings_mh_tabs" value="<?php echo 'Save Settings'; ?>"  class="button-primary" /></p>
</form>