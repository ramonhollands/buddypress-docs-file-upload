<?php 
/*
Plugin Name: MH Uploads
Plugin URI: http://www.mhaseeb.com
Description: Custom Upload for Wiki Editor
Author: M Haseeb
Version: 0.1
Author URI: http://www.mhaseeb.com/
*/

include "download.php";

global $mh_upload_path;
$mh_upload_path = plugin_dir_path(__FILE__);


function mh_group_uploads_menu(){
	if (function_exists('add_menu_page')) {
		add_menu_page('Group Uploads','Group Uploads', 10, 'mh_group_uploads', 'mh_group_uploads', plugins_url('mh-uploads/images/mh-tabs.png'));	
	}
}


function  get_mh_upload_settings() {
	global $mh_upload_path; 
	$handle = @fopen($mh_upload_path . "/mh_uploads_settings.dat", "r");
	$msgz = array();
	while (!feof($handle)) {
	$msg = fgets($handle);
	$msg = explode("=",$msg);
	$msgz[$msg[0]] =  stripslashes($msg[1]);
	}
	fclose($handle);
	return $msgz;
}

function save_mh_upload_settings($msgz) {
	global $mh_upload_path;
	$trows = count($msgz);
	$data='';	$i=0;
	foreach ($msgz as $key => $value) { 
		if($i<$trows-1) {
			$data .= $key."=".$value."\n"; 
		} else { 
			$data .= $key."=".$value; 
		}
		$i++;
	}
	$myFile = $mh_upload_path . "/mh_uploads_settings.dat";
	$fh = fopen($myFile, 'w') or die("can't open file");
	fwrite($fh, $data);
	fclose($fh);
}

add_action('admin_menu', 'mh_group_uploads_menu');	

function mh_group_uploads() {
    include "mh-uploads-page.php";
}

function add_mh_upload_button() {
   if ( get_user_option('rich_editing') == 'true') {
     add_filter('mce_external_plugins', 'add_mh_upload_tinymce_plugin');
     add_filter('mce_buttons', 'register_mh_upload_button');
   }
}

add_action('init', 'add_mh_upload_button');

function register_mh_upload_button($buttons) {
   array_push($buttons, "|", "mhupload");
   return $buttons;
}

function add_mh_upload_tinymce_plugin($plugin_array) {
   $plugin_array['mhupload'] = plugins_url('mh-uploads/mh-uploads.js');
   return $plugin_array;
}


function my_refresh_mce($ver) {
  $ver += 3;
  return $ver;
}
add_filter( 'tiny_mce_version', 'my_refresh_mce' );

/********** Rewriting Rules ***********/
//add_filter( 'rewrite_rules_array', 'my_insert_rewrite_rules' );
add_filter( 'query_vars', 'my_insert_query_vars' );


register_activation_hook( __FILE__, 'init_mh_uploads' );

function init_mh_uploads() {
	$rules = get_option( 'rewrite_rules' );
	if ( ! isset( $rules['(project)/(\d*)$'] ) ) {
		global $wp_rewrite;
	   	$wp_rewrite->flush_rules();
	}
}

// Adding a new rule
function my_insert_rewrite_rules( $rules )
{
	$newrules = array();
	$newrules['downloads/(.+?)/(.+?)/(.+?)/(.+?)$'] = 'index.php?action=mh_download_file&mhdw=$matches[4]&dwid=$matches[3]&dwy=$matches[1]&dwm=$matches[2]&gpid=';
	return $newrules + $rules;
}

function add_rewrite_rules() 
{
    global $wp_rewrite;
    $new_rules = array(
        'downloads/(.+?)/(.+?)/(.+?)/(.+?)$' => 'index.php?action=mh_download_file' .
        '&mhdw=' . $wp_rewrite->preg_index(4) .
        '&dwid=' . $wp_rewrite->preg_index(3) .
        '&dwy=' . $wp_rewrite->preg_index(1) .
        '&dwm=' . $wp_rewrite->preg_index(2) .
        '&gpid='
    );
    // Always add your rules to the top, to make sure your rules have priority
    $wp_rewrite->rules = $new_rules + $wp_rewrite->rules;
}
add_action( 'generate_rewrite_rules', 'add_rewrite_rules' );
add_action( 'init', 'flush_rewrite_rules' );

// Adding the id var so that WP recognizes it
function my_insert_query_vars( $vars )
{
    array_push($vars, 'mhdw');
    array_push($vars, 'action');
    array_push($vars, 'dwy');
    array_push($vars, 'dwm');
    array_push($vars, 'gpid');
    array_push($vars, 'dwid');
    return $vars;
}

/*add_action( 'init', 'generate_mh_upload_rewrite_rules' );  
function generate_mh_upload_rewrite_rules() {  
     add_rewrite_rule(  
        "",  
        "",  
        "top");
flush_rewrite_rules();
} */ 


function mh_upload_styles() {
    // Yuri changed
    global $bp, $wp_query;
    echo '<style type="text/css"> #loading {display:none;} .error {color:#900;} .success, .success a {color:#009900;} .mh_file_head { background: url('.WP_PLUGIN_URL.'/mh-uploads/fico.jpg) no-repeat; width: 29px; height: 31px; display: block; float: left; }  .mh_file_tail { background: url('.WP_PLUGIN_URL.'/mh-uploads/fico2.jpg) no-repeat; width: 32px; height: 31px; display: block; float: left; }  .mh_file_bg{ background: url('.WP_PLUGIN_URL.'/mh-uploads/fbg.jpg) repeat-x; height: 31px; width: auto; display: block; float: left; padding: 0 10px; line-height: 31px; }  .mh-file { line-height: 31px; display: block; }</style>';
    //if ($bp->current_component == 'groups' && $bp->current_action == 'docs') {
    if ($wp_query->query_vars['post_type'] == 'bp_doc') {
        //echo '<script src="http://code.jquery.com/jquery-latest.js"></script>';
        echo '<script src="'.WP_PLUGIN_URL.'/mh-uploads/jquery.fineuploader-3.0.js"></script>';
        echo '<script src="'.WP_PLUGIN_URL.'/mh-uploads/fineuploadform.js"></script>';
        echo '<link href="'.WP_PLUGIN_URL.'/mh-uploads/fineuploader.css" type="text/css" rel="stylesheet">';
    }
    //echo '<script src="'.WP_PLUGIN_URL.'/mh-uploads/jquery.lightbox_me.js"></script>';
}
add_action('wp_head', 'mh_upload_styles');

function mh_upload_footer_div() {
    global $bp;
    $settings = get_mh_upload_settings(); 
    // Yuri changed
    echo '<div id="mh-uploads-box" style="width: 450px; height: auto; background: #fff; display: none;border-radius: 3px; border:1px solid #DEDEDE; overflow:hidden;">
    <div style="background: #000;color: #fff; padding: 5px 10px; margin-bottom: 10px; height: 25px; font-weight: bold; line-height: 25px; font-size: 13px;">'.$settings['POPUP_TITLE'].'<span style="float: right; padding: 3px; background: #676767;  height: 10px; line-height: 9px; margin-top: 5px; border-radius: 3px;"><a href="#" style="text-decoration: none; color: #fff;" onclick="$(\'#mh-uploads-box\').trigger(\'close\'); return false;">x</a></span></div>
    <div id="triggerUpload" class="btn btn-primary" style="margin-top: 10px;">
      <i class="icon-upload icon-white"></i> Upload nu
    </div>
    <div id="manual-fine-uploader"></div>
    <div style="clear: both;"></div>
    <input id="docfile_path" type="hidden" value="'.get_bloginfo('url').'/wp-admin/admin-ajax.php?action=mh_uploads_ajax&gpid='.$bp->groups->current_group->id.'" />
    </div>';
}
//add_action('wp_footer', 'mh_upload_footer_div');

include "uploaded.php";